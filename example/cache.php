<?php
$access_token = "dkswruy78y87y";
$expires_in = 900;
$cacheDir = __DIR__."/../storage";
if (!file_exists($cacheDir) || !is_dir($cacheDir)) {
    mkdir($cacheDir, 0755);
}

$cacheFile = "{$cacheDir}/bd.json";
if (!file_exists($cacheFile) || !is_dir($cacheFile)) {
    fopen($cacheFile, "a");
}

$save = function ($cacheFile, $cache) {
    $saveCache = fopen($cacheFile, "w");
    fwrite($saveCache, json_encode($cache, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    fclose($saveCache);
};

$cacheArr = json_decode(file_get_contents($cacheFile));
$cache = (array)$cacheArr;

if (!array_key_exists("access_token", $cache) || $cache['expires_in'] <= time()) {
    $cache = [
        "access_token" => $access_token,
        "expires_in" => $expires_in + time(),
        "time" => time()
    ];
    $save($cacheFile, $cache);
}

