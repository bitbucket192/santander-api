<?php

require __DIR__."/../vendor/autoload.php";

use Api\Santander\Auth;
use Api\Santander\Workspace;
use Api\Santander\BankSlip;

$clientId = 'tio5DxFFkAgj0uFakxEGf7HTCbOKTiwJ';

//auth
$fields = [
    'client_id' => $clientId,
    'client_secret' => '0qCEKbXQX8qUUhzg',
    'grant_type' => 'client_credentials'
];

$headers = [
    'Content-Type' => 'application/x-www-form-urlencoded'
];

$certDir = realpath(__DIR__."/../storage/credentials/certificado.crt");
$keyDir = realpath(__DIR__."/../storage/credentials/public.key");

//auth
$auth = new Auth(
    'https://trust-sandbox.api.santander.com.br/auth/oauth/v2',
    $certDir,
    $keyDir,
    true
);

//cache
$cacheDir = __DIR__."/../storage";
if (!file_exists($cacheDir) || !is_dir($cacheDir)) {
    mkdir($cacheDir, 0755);
}

$cacheFile = "{$cacheDir}/bd.json";
if (!file_exists($cacheFile) || !is_dir($cacheFile)) {
    fopen($cacheFile, "a");
}

$save = function ($cacheFile, $cache) {
    $saveCache = fopen($cacheFile, "w");
    fwrite($saveCache, json_encode($cache, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    fclose($saveCache);
};

$cacheArr = json_decode(file_get_contents($cacheFile));
$cache = (array)$cacheArr;

if (!array_key_exists("access_token", $cache) || $cache['expires_in'] <= time()) {
    //verify token
    $result = $auth->auth($fields, $headers)->response();
    $cache = [
        "access_token" => $result->access_token,
        "expires_in" => $result->expires_in + time()
    ];
    echo "Autorizando ...";
    $save($cacheFile, $cache);
}

//lista possveis erros
//200 - Requisição bem-sucedida
//400 - Erro de informação do cliente
//500 - Erro de Servidor, Aplicação está fora
//E cada um deles pode se desdobrar em outros mais específicos:
//201 - Requisição bem-sucedida, recurso criado
//204 - O servidor atendeu à solicitação com êxito e que não há conteúdo adicional
//401 - Não autorizado/Autenticado
//403 - Não Autorizado
//404 - Informação não encontrada
//406 - O recurso de destino não possui uma representação atual que seria aceitável
//409 - A solicitação não pode ser concluída devido a um conflito do recurs



$workData = [
    'id' => '305c9cd8-d4f9-4d41-ba1f-df25e8b1717e',
    'type' => 'BILLING',
    'description' => 'Minha Carteira',
    'covenants' => [
        'code' => '0126520'
    ],
    'webhookURL' => 'https://www.suaurl.com.br/',
    'bankSlipBillingWebhookActive' => true,
    'pixBillingWebhookActive' => true
];


$headersDefault = [
    'X-Application-Key' => $clientId,
    'Content-Type' => 'application/json',
    'Authorization' => "Bearer {$cache['access_token']}"
];


$workspace = new Workspace(
    'https://trust-sandbox.api.santander.com.br/collection_bill_management/v2',
    $certDir,
    $keyDir
);

//create
//$workspace->create($workData, $headersDefault);

//show all
//$workspace->findAll($workData, $headersDefault);

//find id
//$workspace->findById($workData, $headersDefault, 'fc251998-6912-4a7d-871f-aa513fceb32d');

//delete
//$workspace->delete($fields = [], $headersDefault, 'fc251998-6912-4a7d-871f-aa513fceb32d');



//register boleto
$workspaceId = "fc251998-6912-4a7d-871f-aa513fceb32d";

$bankData = [
    "nsuCode" => "12345678901234567000",
    "nsuDate" => "2023-07-04",
    "environment" => "TESTE",
    "covenantCode" => "1234567",
    "issueDate" => "2023-07-04",
    "dueDate" => "2023-08-14",
    "bankNumber" => "6030",
    "clientNumber" => "registro 123",
    "nominalValue" => "10.00",
    "payer" => [
        "name" => "João da Silva e Silva",
        "documentType" => "CPF",
        "documentNumber" => "94620639079",
        "address" => "rua nove de janeiro",
        "neighborhood" => "bela vista",
        "city" => "sao paulo",
        "state" => "SP",
        "zipCode" => "05134-897"
    ],
    "documentKind" => "DUPLICATA_MERCANTIL",
    "paymentType" => "REGISTRO",
    "messages" => [
        "mensagem um",
        "mensagem dois"
    ],
    "key" => [
        "type" => "EMAIL",
        "dictKey" => "suachavepix@email.com.br"
    ],
    "discount" => [
        "type" => "VALOR_DATA_FIXA",
        "discountOne" => [
            "value" => 1.5,
            "limitDate" => "2023-07-10"
        ],
        "discountTwo" => [
            "value" => 1,
            "limitDate" => "2023-08-10"
        ]
    ],
    "finePercentage" => "10.00",
    "FineDate" => "2023-08-14",
    "interestPercentage" => "30.00",
    "writeOffQuantityDays" => "1",
    "deductionValue" => "0.10"
];


$bankSlip = new BankSlip(
    "https://trust-sandbox.api.santander.com.br/collection_bill_management/v2/workspaces/{$workspaceId}",
    $certDir,
    $keyDir
);

//$bankSlip->create($bankData, $headersDefault);

//update boleto

//covenantCode *
//bankNumber *

$updateData = [
    "covenantCode" => "1234567",
    "bankNumber" => "1042",
    "operation" => "BAIXAR"
];
//$bankSlip->update($updateData, $headersDefault);

//nsuCode.nsuDate.environment.covenantCode.bankNumber
$bankSlipId = "123.2022-12-12.T.1234567.123";
$bankSlip->findById([], $headersDefault, $bankSlipId);

echo "<pre>";
var_dump($bankSlip->response());


