<?php

namespace Api\Santander;

/**
 * Class Workspace
 * @package Api\Santander
 */
class Workspace extends Api
{
    /**
     * @param string $apiurl
     * @param $certDir
     * @param $keyDir
     */
    public function __construct(string $apiurl, $certDir,  $keyDir)
    {
        parent::__construct($apiurl, $certDir, $keyDir);
    }

    /**
     * @param array $fields
     * @param array $headers
     * @return $this
     */
    public function create(array $fields, array $headers)
    {
        $this->request(
            "POST",
            "workspaces",
            $fields,
            $headers
        );

        return $this;
    }

    /**
     * @param array $fields
     * @param array $headers
     * @param string $id
     * @return $this
     */
    public function findById(array $fields, array $headers, string $id)
    {
        $this->request(
            "GET",
            "workspaces/{$id}",
            $fields,
            $headers
        );

        return $this;
    }

    /**
     * @param array $fields
     * @param array $headers
     * @return $this
     */
    public function findAll(array $fields, array $headers)
    {
        $this->request(
            "GET",
            "workspaces",
            $fields,
            $headers
        );

        return $this;
    }

    /**
     * @param array $fields
     * @param array $headers
     * @param string $id
     * @return $this
     */
    public function delete(array $fields, array $headers, string $id)
    {
        $this->request(
            "DELETE",
            "workspaces/{$id}",
            $fields,
            $headers
        );

        return $this;
    }

}