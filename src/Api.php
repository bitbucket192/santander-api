<?php

namespace Api\Santander;

/**
 * Class Api
 * @package Api\Santander
 */
abstract class Api
{
    /**
     * @var string
     */
    private $apiUrl;
    /**
     * @var array
     */
    private $headers;
    /**
     * @var array
     */
    private $fields;
    /**
     * @var string
     */
    private $endpoint;
    /**
     * @var string
     */
    private $method;
    /**
     * @var object
     */
    private $response;

    /**
     * @var path
     */
    private $certDir;
    /**
     * @var path
     */
    private $keyDir;

    /**
     * @var bool
     */
    private $authentication;

    /**
     * @param string $apiurl
     * @param $certDir
     * @param $keyDir
     */
    public function __construct(string $apiurl,  $certDir, $keyDir, $auth = false)
    {
        $this->apiUrl = $apiurl;
        $this->certDir = $certDir;
        $this->keyDir = $keyDir;
        $this->authentication = $auth;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array|null $fields
     * @param array|null $headers
     * @return void
     */
    public function request(string $method, string $endpoint, array $fields = null, array $headers = null): void
    {
        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->fields = $fields;
        $this->headers($headers);

        $this->dispatch();
    }

    /**
     * @return object|null
     */
    public function error()
    {
        if (!empty($this->response->errors)) {
            return $this->response->errors;
        }

        return null;
    }
    /**
     * @return object | null
     */
    public function response()
    {
        return $this->response;
    }

    /**
     * @param array|null $headers
     * @return void
     */
    private function headers(?array $headers): void
    {
        if (!$headers) {
            return;
        }

        foreach ($headers as $key => $header) {
            $this->headers[] = "{$key}: {$header}";
        }
    }

    /**
     * @return void
     */
    private function dispatch(): void
    {

        if ($this->authentication) {
            $this->fields = (!empty($this->fields) ? http_build_query($this->fields) : null);
        } else {
            $this->fields = (!empty($this->fields) ? json_encode($this->fields) : null);
        }

        $curl = curl_init();

        $option = array(
            CURLOPT_URL => "{$this->apiUrl}/{$this->endpoint}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSLCERT => $this->certDir,
            CURLOPT_SSLKEY => $this->keyDir,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_POSTFIELDS => $this->fields,
            CURLOPT_HTTPHEADER => $this->headers
        );

        curl_setopt_array($curl,$option);
        $this->response = json_decode(curl_exec($curl));
        curl_close($curl);

    }
}
