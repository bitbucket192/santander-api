<?php

namespace Api\Santander;

class Auth extends Api
{
    public function __construct(string $apiurl, $certDir = null, $keyDir = null, $auth)
    {
        parent::__construct($apiurl, $certDir, $keyDir, $auth);
    }

    public function auth(array $fields, array $headers)
    {
        $this->request(
            "POST",
            "/token",
            $fields,
            $headers
        );

        return $this;
    }
}