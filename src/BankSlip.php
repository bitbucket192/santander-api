<?php

namespace Api\Santander;

/**
 * Class BankSlip
 * @package Api\Santander
 */
class BankSlip extends Api
{
    /**
     * @param string $apiurl
     * @param $certDir
     * @param $keyDir
     * @param $auth
     */
    public function __construct(string $apiurl, $certDir, $keyDir, $auth = false)
    {
        parent::__construct($apiurl, $certDir, $keyDir, $auth);
    }

    /**
     * @param array $fields
     * @param array $headers
     * @param string $bankSlipId
     * @return $this
     */
    public function findById(array $fields, array $headers, string $bankSlipId)
    {
        $this->request(
            "GET",
            "bank_slips/{$bankSlipId}",
            $fields,
            $headers
        );

        return $this;
    }

    /**
     * @param array $fields
     * @param array $headers
     * @return $this
     */
    public function create(array $fields, array $headers)
    {
        $this->request(
            "POST",
            "bank_slips",
            $fields,
            $headers
        );

        return $this;
    }

    /**
     * @param array $fields
     * @param array $headers
     * @return $this
     */
    public function update(array $fields, array $headers)
    {
        $this->request(
            "PATCH",
            "bank_slips",
            $fields,
            $headers
        );

        return $this;
    }

}